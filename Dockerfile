FROM python:3.10-slim
RUN apt-get update && apt-get install --no-install-recommends -y bash nano gcc pkg-config default-libmysqlclient-dev

RUN mkdir /src
WORKDIR /src
COPY . /src/

RUN pip install --upgrade pip
RUN pip install -r requirements/prod.txt

RUN python manage.py collectstatic --settings=mom3api.settings.dev --noinput

# run gunicorn
CMD exec gunicorn mom3api.wsgi:application --bind 0.0.0.0:8000 --workers 4 --timeout 300


