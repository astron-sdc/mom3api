# mom3api
A REST API wrapping the mom3 database

 * test system: https://sdc-dev.astron.nl/mom3api/
 * production : https://sdc.astron.nl/mom3api/


## description
Mom3 is the project/observation administration for LOFAR before TMSS took over that role in 2023.
The MoM technology is old, it started 20 years ago as a suite of web applications in Java.
The framework we used is Java/Struts/Hibernate, running on a Tomcat webserver. The database is MySQL.

MoM development and maintance was abandoned years ago, and since 2023 MoM is no longer in the observation chain.
However, we still need access to the MoM (MySQL) database, because it holds some unique metadata that isn't in the LTA.
The MoM application itself does not provide all the endpoint we now need. 
Since the MoM application is no longer maintained, we cannot add extra endpoints.
And it is not unthinkable that the MoM application will be switched off because of security issues.

This REST API provides access to the MoM database without any dependence on the MoM3 application or infrastructure.

## how it was done

### 2018 experiment
In 2018, as an experiment, the Django models were automatically generated from the MoM database and then cleaned up to get a working datamodel.
See: https://github.com/vermaas/mom3api/tree/original_2018

The MoM database has a complex datamodel that evolved over its many years of operations.
This image is an example of the 2009 datamodel, which has evolved further until 2016 

![](docs/mom3_datamodel_2023.png)

## sql tables and sql views
The MoM database contains both 'tables' and 'sql views'. 
(in MySQL they are called 'views', but to prevent confusion with the Django 'views' I will refer to them as 'sql views')

The 'sql views' are used by external applications, like BIRT viewer.
The automatic conversion to Django models did not work for views, so they were omitted. 
Only the 'tables' are mapped in `models.py`.

## more models than views
The full database structure of MoM is made available in the `models.py`
But only a very limited set of `views` (API endpoints) were added. 
Because initially this was only a timeboxed experiment and because currently we only have a limited use case.

But because we have the full MoM datamodel available in the `models.py`, 
REST API endpoints can be easily expanded in the future when the need arises.


## readonly
Django normally manages the databases it accesses, so it can use the ORM (Object Relational Mapping).
In this case we do not want that, because the MoM database is managed by the MoM application, and should not be altered externally.

There are 3 layers of protection added to prevent accidental changes to the original MoM database.
(that sounds paranoid, but since we lost the ability to repair damage to MoM, it is crucial that we avoid it)

### layer 1 - managed = False

All the models in this application are therefore set to 'managed = False', indicating that the Django ORM doesn't touch them.

```json
    class Meta:
        managed = False
        db_table = 'dataproduct'
```


### layer 2 - momreadonly account

This is not enough, a `migrate` command by a developer would still add extra tables to the MoM database, like a `user` table.
This may or may not cause problems for the MoM application, but on principle, we do not want to alter the MoM database.
So we use the `momreadonly` account that is defined in the `mom3_lofar` database itself. 
That account only has 'select' rights

note: 
future use-cases may require small changes/repairs to be made in the MoM data. 
If that happens, then we should start using a different database account with proper rights.

### layer 3 - readonly middleware
A third party middleware library was added that can declare a database 'readonly' from the Django side.
That was enabled for the test and production instances (see settings/docker.py).
We have no practical experience with this middleware
 
https://pypi.org/project/django-db-readonly/


----
# use cases

## [1] get dataproduct id by its name. 

This functionality does not exist in the MoM application, and the results from the LTA are sometimes wrong.

This can be done in 2 ways
* with a dedicated 'command' endpoint (recommended)
* with a filter on the REST API (slower)

### using command endpoint

This same functionality as a fast endpoint:
https://sdc.astron.nl/mom3api/get-dataproduct-ids?name=L770733_SAP000_B017_P000_bf.tar
```json
{"ids": [28215535]}
```

### using API endpoints
Example: retrieve a dataproduct by its name by using a filter:
https://sdc.astron.nl/mom3api/dataproducts/?name=L770733_SAP000_B017_P000_bf.tar

```json
        {
            "id": 28215535,
            "name": "L770733_SAP000_B017_P000_bf.tar",
            "indexid": 17,
            "timestamp": "2020-03-04T14:16:52Z",
            "type": "LOFAR_PULSAR",
            "released": 0,
            "exported": 0,
            "fileformat": "PULP",
            "archived": 0,
            "pipelined": 0,
            "topology": null,
            "status": null,
            "placeholder": 0,
            "status_code": 0,
            "message": null,
            "mom2objectid": "http://localhost:8000/mom3api/mom2objects/983655/"
        }

```
## [2] get list of dataproduct id's by sas_id
This can be used as alternative for the MoM command `getmom2DPIds` command.
This MoM command is currently used in the ldv archiver, see https://support.astron.nl/confluence/display/SDCP/mom3api

You can compare the results of the MoM command and mom3api request.

| service | url                                                                                    |
| :--- |:---------------------------------------------------------------------------------------| 
| MoM | https://lofar.astron.nl/mom3/interface/importXML2.do?command=getmom2DPIds&sasid=770731 |
| mom3api | https://sdc.astron.nl/mom3api/get-dataproduct-ids/?sas_id=770731                       |


----

## [3] Navigable REST API for discovery/troubleshooting
![](docs/mom3_django_screenshot1.png)