"""
common helper functions
"""
import logging

from mom3.models import Dataproduct, LofarObservation, LofarPipeline, LofarDataproduct, Mom2Object

logger = logging.getLogger(__name__)


def get_archive_id(dp):
    """
    this simulates some old MoM trickery

    logic based on a SQL migration file from the MoM repo:

    add_dataproduct_archiveid_5jun2015.sql
    ALTER TABLE lofar_dataproduct ADD COLUMN archive_id int(11) default 0;
    UPDATE lofar_dataproduct SET archive_id = mom2_dp_id WHERE mom2_dp_id > 0;
    UPDATE lofar_dataproduct SET archive_id = dataproductid + 1000000 WHERE mom2_dp_id < 0;

    :param db: dataproduct object
    :return: dataproduct_id
    """

    ldp = LofarDataproduct.objects.get(dataproductid=dp.id)

    # part of the dataproducts in MoM have a value for the 'archive_id', if that is the case then use that
    if ldp.archive_id > 0:
        return ldp.archive_id

    # when 'archive_id == 0' and the (old) mom2_dp_id < 0, then increment it with 1000000
    # because the SQL database fix was only done once, for dataproducts before 5 jun 2015
    # and the INCREMENT is the 'new normal' since then.
    if ldp.mom2_dp_id < 0:
        return dp.id + 1000000

    # default, the old situation before the 'milion gap' was created
    return dp.id


def get_dataproduct_ids_by_name(name):
    """
    Get a dataproducts by filename and return a list of id's. (ideally only 1 id).
    example: /mom3api/get-dataproduct-id?name=L770733_SAP000_B017_P000_bf.tar
    :return ids, a list of id's of dataproducts, ideally this list contains a single id.
    """
    list = []
    dps = Dataproduct.objects.filter(name=name)
    for dp in dps:
        dataproduct_id = get_archive_id(dp)
        list.append(dataproduct_id)
    return list


def get_dataproduct_ids_by_sas_id(sas_id):
    """
    Get a dataproducts by sas_id
    example pipeline: /mom3api/get-dataproduct-id?sas_id=770733
    test against: https://lofar.astron.nl/mom3/interface/importXML2.do?command=getmom2DPIds&sasid=770733

    example observation: /mom3api/get-dataproduct-id?sas_id=770731
    test against: https://lofar.astron.nl/mom3/interface/importXML2.do?command=getmom2DPIds&sasid=770731

    :return ids, a list of id's of dataproducts, ideally this list contains a single id.

    see original java implementation in MoM:
    https://svn.astron.nl/ROD/trunk/Mom-parent/Mom3/src/nl/astron/mom2/business/xml/ExportXMLDelegate.java

	public Integer getArchiveId() {
		// the archiveId was not filled in in the initial database transaction that created the dataproduct.
		// so the first time is is accessed, fill it in anyway... and store.
		if (mom2DPId0==-1) { //nv 23 feb 2016, to make negative ID's possible
			archiveId = mom2DPId;
		} else {
			archiveId = mom2DPId0;
		}

		return archiveId;
	}

	//nv:27mar2015
	public Document getMom2DPIDsDocument(String sasId,UserAccount userAccount) {

		try {
			Integer id = Integer.valueOf(sasId);

			List<DataProduct> dps = DelegateFactory.getMom2ObjectDelegate().getDataProductsBySasId(id, userAccount);

			XMLBuilder xmlBuilder;
			try {
				xmlBuilder = new XMLBuilder();
			} catch (ParserConfigurationException e) {
				throw new InterfaceException(e);
			}
			addNamespaces(xmlBuilder);

			Element elementRoot = xmlBuilder.addRootElement(XMLConstants.MOM2_NAMESPACE, XMLConstants.MOM2DPIDs);
			Element elementDataproducts = xmlBuilder.addElementWithAttribute(elementRoot, "outputdataproducts", "sasid", sasId);


			for (DataProduct dp : dps) {
				LofarDataProduct ldp = (LofarDataProduct) dp;

				Integer mom2dpid = ldp.getArchiveId(); //nv: 4 jun 2015. this returns the old mom_dp_id for older dataproducts, and the new id for the newer dataproducts

				xmlBuilder.addTextElement(elementDataproducts, XMLConstants.MOM2DPID, mom2dpid);
			}
			return xmlBuilder.getDocument();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

    // nv3nov2011
	@Override
	public List<DataProduct> getDataProductsBySasId(Integer sasId, UserAccount userAccount) {
		List<DataProduct> dps = new ArrayList<DataProduct>();

		// is this an observation or a pipeline
		LofarObservation lofarObservation = this.getLofarObservationBySasId(sasId, userAccount);
		if (lofarObservation!=null) {
			List<Mom2Object> beams = lofarObservation.getBeamMeasurements();
			for (Mom2Object beam : beams) {
				for (DataProduct dp : beam.getOutputDataProducts()) {
					if (!dp.isPlaceHolder()) {
						dps.add(dp);
					}
				}
				// dps.addAll(beam.getOutputDataProducts()); // including placeholders

			}
		}

		LofarPipeline lofarPipeline = getPipelineBySasId(sasId, userAccount);
		if (lofarPipeline!=null) {
			for (DataProduct dp : lofarPipeline.getOutputDataProducts()) {
				if (!dp.isPlaceHolder()) {
					dps.add(dp);
				}
			}
			//dps.addAll(lofarPipeline.getOutputDataProducts());  // including placeholders
		}

		return dps;
	}
    """
    list = []

    # does this sas_id belong to an observation or a pipeline?
    # pipelines have dataproducts directly associated with them
    # observations have an additional layer of 'beams' that hold the dataproducts

    sasid = int(sas_id)

    try:
        observation = LofarObservation.objects.get(observation_id=sasid)

        # get the beams
        mom2objectid = observation.mom2objectid.id
        mom2object = Mom2Object.objects.get(id=mom2objectid)
        beams = Mom2Object.objects.filter(parentid=mom2object.id)
        for beam in beams:
            mom2objectid = beam.id
            dps = Dataproduct.objects.filter(mom2objectid=mom2objectid)
            for dp in dps:
                # only add 'real' dataproducts, not the placeholders
                if dp.placeholder == 0:
                    dataproduct_id = get_archive_id(dp)
                    list.append(dataproduct_id)

    except Exception as error:
        # it is not an observation, is it ia pipeline?
        try:
            pipeline = LofarPipeline.objects.get(pipeline_id=sasid)
            mom2objectid = pipeline.mom2objectid.id
            mom2object = Mom2Object.objects.get(id=mom2objectid)
            dps = Dataproduct.objects.filter(mom2objectid=mom2object.id)
            for dp in dps:
                # only add 'real' dataproducts, not the placeholders
                if dp.placeholder == 0:
                    dataproduct_id = get_archive_id(dp)
                    list.append(dataproduct_id)

        except:
            # no dataproducts found, for whatever reason.
            # Return an empty list and let the requester deal with that.
            pass

    return list

