from django.test import TestCase
from mom3.models import Dataproduct, LofarDataproduct
from mom3.services.algorithms import get_dataproduct_ids_by_name

class GetDataproductIdsByNameTest(TestCase):
    def setUp(self):
        # Create some sample Dataproduct objects for testing


        timestamp = "2023-12-22 13:16:00"
        self.dp1 = Dataproduct.objects.create(name="L770733_SAP000_B017_P000_bf.tar", timestamp=timestamp)
        self.ldp1 = LofarDataproduct.objects.create(dataproductid=self.dp1, mom2_dp_id=-1, archive_id=111)

        self.dp2 = Dataproduct.objects.create(name="duplicate_file.tar", timestamp=timestamp)
        self.ldp2 = LofarDataproduct.objects.create(dataproductid=self.dp2, mom2_dp_id=-1, archive_id=222)

        self.dp3 = Dataproduct.objects.create(name="duplicate_file.tar", timestamp=timestamp)
        self.ldp3 = LofarDataproduct.objects.create(dataproductid=self.dp3, mom2_dp_id=-1, archive_id=333)

    def test_get_dataproduct_ids_single_match(self):
        # Test case where there is a single matching Dataproduct
        name = "L770733_SAP000_B017_P000_bf.tar"

        # Call the function to be tested
        result = get_dataproduct_ids_by_name(name)

        # Check if the result is a list containing a single id, the archive id
        self.assertEqual(result, [111])

    def test_get_dataproduct_ids_multiple_matches(self):
        # Test case where an empty name is provided
        name = "duplicate_file.tar"

        # Call the function to be tested
        result = get_dataproduct_ids_by_name(name)

        # Check if the archive_id's are returned
        self.assertEqual(result, [222, 333])

    def test_get_dataproduct_ids_no_match(self):
        # Test case where there is no matching Dataproduct
        name = "nonexistent_file.tar"

        # Call the function to be tested
        result = get_dataproduct_ids_by_name(name)

        # Check if the result is an empty list
        self.assertEqual(result, [])

    def test_get_dataproduct_ids_empty_name(self):
        # Test case where an empty name is provided
        name = ""

        # Call the function to be tested
        result = get_dataproduct_ids_by_name(name)

        # Check if the result is an empty list
        self.assertEqual(result, [])
