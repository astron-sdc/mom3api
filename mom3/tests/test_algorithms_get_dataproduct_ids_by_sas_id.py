from django.test import TestCase
from mom3.models import Dataproduct, Mom2Object, LofarPipeline, LofarObservation, LofarDataproduct
from mom3.services.algorithms import get_dataproduct_ids_by_sas_id

class GetDataproductIdsBySasIDTest(TestCase):
    def setUp(self):
        # Create some sample Dataproduct objects for testing

        # setup pipeline with 3 dataproducts
        self.mom2object_pipeline = Mom2Object.objects.create(id=123, mom2id=321)
        self.lofar_pipeline = LofarPipeline.objects.create(pipeline_id=770733, mom2objectid = self.mom2object_pipeline)

        timestamp = "2023-12-22 13:16:00"

        self.dp1 = Dataproduct.objects.create(name="L770733_SAP000_B017_P000_bf.tar", timestamp=timestamp,
                                              mom2objectid=self.mom2object_pipeline, placeholder=0)
        self.ldp1 = LofarDataproduct.objects.create(dataproductid=self.dp1, mom2_dp_id=-1, archive_id=123)
        self.dp2 = Dataproduct.objects.create(name="duplicate_file.tar", timestamp=timestamp,
                                              mom2objectid=self.mom2object_pipeline, placeholder=0)
        self.ldp2 = LofarDataproduct.objects.create(dataproductid=self.dp2, mom2_dp_id=456, archive_id=0)
        self.dp3 = Dataproduct.objects.create(name="duplicate_file.tar", timestamp=timestamp,
                                              mom2objectid=self.mom2object_pipeline, placeholder=0)
        self.ldp3 = LofarDataproduct.objects.create(dataproductid=self.dp3, mom2_dp_id=-1, archive_id=0)

        # setup observation with 2 beams with both 3 dataproducts of which 1 is a placeholder
        self.mom2object_observation = Mom2Object.objects.create(id=456, mom2id=654)
        self.lofar_observation = LofarObservation.objects.create(observation_id=770731,
                                                                 mom2objectid=self.mom2object_observation,
                                                                 tbb_piggyback_allowed=0)

        self.mom2object_beam1 = Mom2Object.objects.create(id=460, mom2id=655, parentid=self.mom2object_observation)
        self.dp4 = Dataproduct.objects.create(name="LOTAAS-P1483C-SAP0.1483.SAP0.obs.cs", timestamp=timestamp,
                                              mom2objectid=self.mom2object_beam1, placeholder=1)
        self.ldp4 = LofarDataproduct.objects.create(dataproductid=self.dp4, mom2_dp_id=-1, archive_id=444)
        self.dp5 = Dataproduct.objects.create(name="L770731_SAP000_B000_S0_P000_bf.h5", timestamp=timestamp,
                                              mom2objectid=self.mom2object_beam1, placeholder=0)
        self.ldp5 = LofarDataproduct.objects.create(dataproductid=self.dp5, mom2_dp_id=-1, archive_id=555)
        self.dp6 = Dataproduct.objects.create(name="L770731_SAP000_B001_S0_P000_bf.h5", timestamp=timestamp,
                                              mom2objectid=self.mom2object_beam1, placeholder=0)
        self.ldp6 = LofarDataproduct.objects.create(dataproductid=self.dp6, mom2_dp_id=-1, archive_id=666)
        self.mom2object_beam2 = Mom2Object.objects.create(id=470, mom2id=656, parentid=self.mom2object_observation)
        self.dp7 = Dataproduct.objects.create(name="LOTAAS-P1483C-SAP0.1483.SAP0.obs.is", timestamp=timestamp,
                                              mom2objectid=self.mom2object_beam1, placeholder=1)
        self.ldp7 = LofarDataproduct.objects.create(dataproductid=self.dp7, mom2_dp_id=-1, archive_id=777)
        self.dp8 = Dataproduct.objects.create(name="L770731_SAP001_B000_S0_P000_bf.h5", timestamp=timestamp,
                                              mom2objectid=self.mom2object_beam2, placeholder=0)
        self.ldp8 = LofarDataproduct.objects.create(dataproductid=self.dp8, mom2_dp_id=-1, archive_id=888)
        self.dp9 = Dataproduct.objects.create(name="L770731_SAP001_B001_S0_P000_bf.h5", timestamp=timestamp,
                                              mom2objectid=self.mom2object_beam2, placeholder=0)
        self.ldp9 = LofarDataproduct.objects.create(dataproductid=self.dp9, mom2_dp_id=-1, archive_id=999)
    def test_get_dataproduct_ids_from_pipeline(self):
        # Test case where there is a single matching Dataproduct
        sas_id = "770733"

        # Call the function to be tested
        result = get_dataproduct_ids_by_sas_id(sas_id)

        # Check if the result is a list containing a single id
        self.assertEqual(result, [123, self.dp2.id, self.dp3.id + 1000000])

    def test_get_dataproduct_ids_from_observation(self):
        # Test case where there is a single matching Dataproduct
        sas_id = "770731"

        # Call the function to be tested
        result = get_dataproduct_ids_by_sas_id(sas_id)

        # Check if the result is a list containing a single id
        self.assertEqual(result, [555, 666, 888, 999])



