from django.test import TestCase, Client
from mom3.models import Dataproduct, LofarDataproduct


class ViewsGetDataproductIdsTest(TestCase):

    def setUp(self):
        self.client = Client()
    def setUp(self):
        # Create some sample Dataproduct objects for testing
        timestamp = "2023-12-22 13:25:00"
        self.dp1 = Dataproduct.objects.create(name="L770733_SAP000_B017_P000_bf.tar", timestamp=timestamp)
        self.ldp1 = LofarDataproduct.objects.create(dataproductid=self.dp1, mom2_dp_id=-1, archive_id=123)

        self.dp2 = Dataproduct.objects.create(name="duplicate_file.tar", timestamp=timestamp)
        self.ldp2 = LofarDataproduct.objects.create(dataproductid=self.dp2, mom2_dp_id=-1, archive_id=456)

        self.dp3 = Dataproduct.objects.create(name="duplicate_file.tar", timestamp=timestamp)
        self.ldp3 = LofarDataproduct.objects.create(dataproductid=self.dp3, mom2_dp_id=-1, archive_id=789)

    def test_get_dataproduct_ids_single_match(self):
        # Define a sample name for testing
        name = "L770733_SAP000_B017_P000_bf.tar"

        # Prepare a GET request to the view
        response = self.client.get('/mom3api/get-dataproduct-ids/', {'name': name})

        # Check if the response status code is 200 (OK)
        self.assertEqual(response.status_code, 200)

        # Parse the response content as JSON, expecting the following structure
        # content = {
        #     "ids" : ids,
        # }
        content = response.json()

        # Check if the expected key "ids" is in the JSON response
        self.assertIn('ids', content)

        # Replace 'your_expected_ids' with the expected result based on your algorithms.get_dataproduct_ids
        your_expected_ids = [123]

        # Check if the returned ids match the expected ids
        self.assertEqual(content['ids'], your_expected_ids)

    def test_get_dataproduct_ids_multiple_matches(self):
        # Define a sample name for testing
        name = "duplicate_file.tar"

        # Prepare a GET request to the view
        response = self.client.get('/mom3api/get-dataproduct-ids/', {'name': name})

        # Check if the response status code is 200 (OK)
        self.assertEqual(response.status_code, 200)

        # Parse the response content as JSON, expecting the following structure
        # content = {
        #     "ids" : ids,
        # }
        content = response.json()

        # Check if the expected key "ids" is in the JSON response
        self.assertIn('ids', content)

        # Replace 'your_expected_ids' with the expected result based on your algorithms.get_dataproduct_ids
        your_expected_ids = [456, 789]

        # Check if the returned ids match the expected ids
        self.assertEqual(content['ids'], your_expected_ids)