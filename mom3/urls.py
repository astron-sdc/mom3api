from django.urls import include, path
from rest_framework.urlpatterns import format_suffix_patterns

# app imports
from . import views

urlpatterns = [
    # main page
    path('', views.IndexView.as_view(), name='index'),

    # --- REST API ---
    path('dataproducts/', views.DataProductListView.as_view(), name='dataproduct-list-view'),
    path('dataproducts/<int:pk>/', views.DataProductDetailView.as_view(), name='dataproduct-detail-view'),
    path('lofardataproducts/', views.LofarDataProductListView.as_view(), name='lofar-dataproduct-list-view'),
    path('lofardataproducts/<int:pk>/', views.LofarDataProductDetailView.as_view(), name='lofar-dataproduct-detail-view'),
    path('projects/', views.ProjectListView.as_view(), name='project-list-view'),
    path('projects/<int:pk>/', views.ProjectDetailView.as_view(), name='project-detail-view'),
    path('mom2objects/', views.Mom2ObjectListView.as_view(), name='mom2object-list-view'),
    path('mom2objects/<int:pk>/', views.Mom2ObjectDetailView.as_view(), name='mom2object-detail-view'),
    path('mom2objectstatus/<int:pk>/', views.Mom2ObjectStatusDetailView.as_view(),
        name='mom2objectstatus-detail-view'),


    # --- command endpoints ---
    # used by LDV archiver to retrieve correct dataproduct id from MoM
    path('get-dataproduct-ids/', views.get_dataproduct_ids, name='get-dataproduct-ids'),

    # stub
    path('getsip/<int:mom2dpid>', views.getsip, name='get-sip'),
]
urlpatterns = format_suffix_patterns(urlpatterns)
