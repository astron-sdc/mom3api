import logging

from rest_framework import generics, pagination
from django_filters import rest_framework as filters
from django.http import JsonResponse
from django.views.generic import ListView

from .models import Project, Mom2Object, Dataproduct, LofarDataproduct, Mom2Objectstatus, LofarBeamMeasurement
from .serializers import ProjectSerializer, Mom2ObjectSerializer, DataproductSerializer, Mom2ObjectStatusSerializer, \
    LofarBeamMeasurementSerializer, LofarDataproductSerializer

from .services import LofarSIPExportXMLDelegate, algorithms

logger = logging.getLogger(__name__)

# ---------- GUI Views -----------
class IndexView(ListView):
    """
    This is the index page of mom3api
    """
    queryset = Project.objects.all()
    template_name = 'mom3/index.html'

# ---------- REST API views ----------
class DataproductFilter(filters.FilterSet):

    class Meta:
        model = Dataproduct

        fields = {
            'id': ['exact', 'icontains'],
            'name': ['exact', 'icontains'],
            'type': ['exact', 'icontains'],
            'mom2objectid__id': ['exact', 'icontains']
        }

class DataProductListView(generics.ListAPIView):
    queryset = Dataproduct.objects.all().order_by('id')
    serializer_class = DataproductSerializer
    filterset_class = DataproductFilter

class DataProductDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Dataproduct.objects.all()
    serializer_class = DataproductSerializer

class LofarDataproductFilter(filters.FilterSet):

    class Meta:
        model = LofarDataproduct

        fields = {
            'mom2_dp_id': ['exact', 'icontains'],
            'group_id': ['exact', 'icontains'],
            'archive_id': ['exact', 'icontains'],
            #'dataproductid': ['exact'],
            #'dataproductid__name': ['exact', 'icontains']
        }

class LofarDataProductListView(generics.ListAPIView):
    queryset = LofarDataproduct.objects.all().order_by('id')
    serializer_class = LofarDataproductSerializer
    filterset_class = LofarDataproductFilter

class LofarDataProductDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = LofarDataproduct.objects.all()
    serializer_class = LofarDataproductSerializer

class ProjectFilter(filters.FilterSet):

    class Meta:
        model = Project

        fields = {
            'id': ['exact', 'icontains'],
        }

class ProjectListView(generics.ListCreateAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    filterset_class = ProjectFilter

class ProjectDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer

class Mom2ObjectFilter(filters.FilterSet):
    class Meta:
        model = Mom2Object

        fields = {
            'id': ['exact', 'icontains'],
            'name': ['exact', 'icontains'],
            'mom2id': ['exact', 'icontains'],
            'group_id': ['exact', 'icontains'],
            'mom2objecttype': ['exact', 'icontains']
        }

class Mom2ObjectListView(generics.ListAPIView):
    queryset = Mom2Object.objects.all()
    serializer_class = Mom2ObjectSerializer
    filterset_class = Mom2ObjectFilter

class Mom2ObjectDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Mom2Object.objects.all()
    serializer_class = Mom2ObjectSerializer

class Mom2ObjectStatusDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Mom2Objectstatus.objects.all()
    serializer_class = Mom2ObjectStatusSerializer



# ---------- Command endpoint views ----------
def getsip(request, mom2dpid = None):

    if not mom2dpid:
        return JsonResponse({"error": "no mom2dpid given"}, status=400)

    try:
        sip = LofarSIPExportXMLDelegate.getsip(mom2dpid)
        content = {
            "mom2dpid" : mom2dpid,
            "sip": sip
        }
        return JsonResponse(content)

    except:
        return JsonResponse(
            {"error": "could not generate SIP"}, status=404
        )



def get_dataproduct_ids(request):
    """
    Search dataproducts by filename and return a list of id's. (ideally only 1 id).
    example: /mom3api/get-dataproduct-id/L770733_SAP000_B017_P000_bf.tar
    :return a json structure containing the ids or an error message
    """
    name = request.GET.get("name")

    if name:
        try:
            ids = algorithms.get_dataproduct_ids_by_name(name)
            content = {
                "ids" : ids,
            }
            return JsonResponse(content)

        except Exception as error:
            return JsonResponse(
                {"error": str(error)}, status=404
            )


    sas_id = request.GET.get("sas_id")
    if sas_id:
        try:
            ids = algorithms.get_dataproduct_ids_by_sas_id(sas_id)
            content = {
                "ids": ids,
            }
            return JsonResponse(content)

        except Exception as error:
            return JsonResponse(
                {"error": str(error)}, status=404
            )


    return JsonResponse({"error": "no name or sas_id given"}, status=400)
