from mom3api.settings.base import *
DEBUG = True

# these settings make it possible to write unit tests for unmanaged models
# the 'meta managed' flag is set to True for testing, so that tables can be created in the test database
IS_TESTING = True
MIGRATION_MODULES = {'mom3': None}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'lofar_mom3',
        'USER': 'root',
        'PASSWORD': 'my_sql_2023_!@#',
        'HOST': 'localhost',   # Or an IP Address that your DB is hosted on
        'PORT': '3306',
    }
}