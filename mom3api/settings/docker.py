import os
from mom3api.settings.base import *

ALLOWED_HOSTS = [ '*' ]
CORS_ORIGIN_ALLOW_ALL = True
SITE_READ_ONLY = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ['DATABASE_NAME'],
        'HOST': os.environ['DATABASE_HOST'],
        'PORT': os.environ['DATABASE_PORT'],
        'USER' : os.environ['DATABASE_USER'],
        'PASSWORD' : os.environ['DATABASE_PASSWORD'],
    },
}

